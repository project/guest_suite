<?php

namespace Drupal\guest_suite;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Defines the Guest Suite review storage.
 */
class ReviewStorage extends SqlContentEntityStorage implements ReviewStorageInterface {

  /**
   * {@inheritDoc}
   */
  public function import(array $remote_values = []): int {
    $reviews = $this->loadByProperties([
      'guestapp_id' => $remote_values['guestapp_id'],
    ]);
    $review = reset($reviews);

    $storage_format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
    $storage_timezone = new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE);
    if (!$review) {
      $review = $this->create([
        'remote_id' => $remote_values['id'],
        'guestapp_id' => $remote_values['guestapp_id'],
        'langcode' => strtolower($remote_values['language_code']),
        'creation_date' => DrupalDateTime::createFromFormat(
            \DateTimeInterface::W3C,
            $remote_values['creation_date'],
            $remote_values['timezone']
        )
          ->setTimezone($storage_timezone)
          ->format($storage_format),
        'authenticity_url' => $remote_values['authenticity_url'],
        'unique_token' => $remote_values['unique_token'],
      ]);
    }

    /** @var \Drupal\guest_suite\Entity\ReviewInterface $review */
    $review->setUserName($remote_values['user_name']);
    $review->setTitle($remote_values['title']);
    $review->setComment($remote_values['comment']);
    $review->setPublicationDate(
      DrupalDateTime::createFromFormat(
        \DateTimeInterface::W3C,
        $remote_values['publication_date'],
        $remote_values['timezone']
      )
        ->setTimezone($storage_timezone)
        ->format($storage_format));
    $review->setExperienceDate(
      DrupalDateTime::createFromFormat(
        'Y-m-d',
        $remote_values['experience_date'],
      )
        ->format(DateTimeItemInterface::DATE_STORAGE_FORMAT)
    );
    $review->setType($remote_values['type']);
    $review->setGlobalRate($remote_values['global_rate']);
    $review->setEstablishmentId($remote_values['establishment_id']);
    $review->setEstablishmentIdGuestapp($remote_values['establishment_id_guestapp']);

    return $review->save();
  }

}
