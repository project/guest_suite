<?php

declare(strict_types=1);

namespace Drupal\guest_suite;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * The Guest Suite API consumer.
 *
 * @package Drupal\guest_suite\Services
 */
class ApiConsumer implements ApiConsumerInterface {

  const API_URL = 'https://wire.guest-suite.com/rest/';

  /**
   * The Guest Suite logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The entity type manager.
   *
   * @var ReviewStorage
   */
  protected $reviewStorage;

  /**
   * The Guest Suite access token.
   *
   * @var string
   */
  protected $accessToken;

  /**
   * Contructs a new ApiConsumer instance.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, ConfigFactory $config_factory, Client $http_client, EntityTypeManagerInterface $entity_type_manager) {
    $this->logger = $logger_factory->get('guest_suite');
    $this->config = $config_factory->getEditable('guest_suite.settings');
    $this->httpClient = $http_client;
    $this->reviewStorage = $entity_type_manager->getStorage('guest_suite_review');
    $this->setAccessToken();
  }

  /**
   * {@inheritDoc}
   */
  public function setAccessToken(?string $access_token = NULL): self {
    $this->accessToken = $access_token ?? $this->config->get('access_token');

    return $this;
  }

  /**
   * Access token getter.
   *
   * @return string
   *   The Guest Suite access token.
   */
  protected function getAccessToken(): string {
    return (string) $this->accessToken;
  }

  /**
   * Gets a endpoint data.
   *
   * @param string $end_point
   *   The endpoint (without extension).
   * @param string $query
   *   Query params.
   *
   * @return array|null
   *   The requested data on success,
   *   NULL on access denied,
   *   an empty array otherwise.
   */
  protected function getEndpointData(string $end_point, $query = []): ?array {
    try {
      $response = $this->httpClient->request(
        'GET',
        self::API_URL . $end_point . '.json',
        ['query' => $query]
      );
      return Json::decode((string) $response->getBody());
    }
    catch (ClientException $e) {
      $this->logger->error($e->getMessage());
      return NULL;
    }

    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getReviewsPerPage(int $page): ?array {
    $data = $this->getEndpointData('reviews', [
      'access_token' => $this->getAccessToken(),
      'page' => $page,
    ]);

    if (!$data) {
      return NULL;
    }

    return $data['reviews'];
  }

  /**
   * {@inheritDoc}
   */
  public function getEstablishments(): ?array {
    return $this->getEndpointData('establishments', [
      'access_token' => $this->getAccessToken(),
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getEstablishment(int $establishment_id_guestapp): ?array {
    return $this->getEndpointData('establishment', [
      'access_token' => $this->getAccessToken(),
      'establishment_id_guestapp' => $establishment_id_guestapp,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getStatistics(): ?array {
    $establishments = $this->getEstablishments();
    if ($establishments && count($establishments) == 1) {
      $establishment = $this->getEstablishment($establishments[0]['establishment_id_guestapp']);
      return [
        'total' => $establishment['total_reviews'],
        'average_rate' => $establishment['average_rate'],
      ];
    }
    return $this->getEndpointData('establishments/statistics', [
      'access_token' => $this->getAccessToken(),
    ]);
  }

}
