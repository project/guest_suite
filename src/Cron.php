<?php

declare(strict_types=1);

namespace Drupal\guest_suite;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\CronInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * Default cron implementation.
 */
class Cron implements CronInterface {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The review manager.
   *
   * @var \Drupal\guest_suite\ApiConsumer
   */
  protected $apiConsumer;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new Cron object.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\guest_suite\ApiConsumer $api_consumer
   *   The Guest Suite API consumer.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   */
  public function __construct(QueueFactory $queue_factory, ApiConsumer $api_consumer, ConfigFactory $config_factory) {
    $this->queueFactory = $queue_factory;
    $this->apiConsumer = $api_consumer;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function run(): void {
    if (!$this->configFactory->get('guest_suite.settings')->get('cron_import')) {
      return;
    }

    // Prevents duplicate queue items from being created.
    $fetch_queue = $this->queueFactory->get('guest_suite_review_fetcher');
    $import_queue = $this->queueFactory->get('guest_suite_review_importer');
    if ($fetch_queue->numberOfItems() > 0 || $import_queue->numberOfItems() > 0) {
      return;
    }

    $statistics = $this->apiConsumer->getStatistics();
    if (empty($statistics['total'])) {
      return;
    }

    $page_total = ceil($statistics['total'] / 100);
    for ($page_index = 0; $page_index < $page_total; $page_index++) {
      $fetch_queue->createItem($page_index);
    }
  }

}
