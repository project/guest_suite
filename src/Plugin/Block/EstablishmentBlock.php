<?php

namespace Drupal\guest_suite\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\guest_suite\ApiConsumer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Guest Suite establishement block.
 *
 * @Block(
 *   id = "guest_suite_establishment",
 *   admin_label = @Translation("Guest Suite establishment"),
 * )
 */
class EstablishmentBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The review manager.
   *
   * @var \Drupal\guest_suite\ApiConsumer
   */
  protected $apiConsumer;

  /**
   * Constructs a new EstablishmentBlock instance.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\guest_suite\ApiConsumer $api_consumer
   *   The review manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ApiConsumer $api_consumer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->apiConsumer = $api_consumer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('guest_suite.api_consumer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    if (!$establishments = $this->apiConsumer->getEstablishments()) {
      $url = Url::fromRoute('guest_suite.configuration')->toString();
      $markup = Markup::create(
        $this->t('No establishment available.') . '<br>' .
        $this->t('Please review <a href="@url" title="Guest Suite settings">your settings</a>.', ['@url' => $url]
      ));
      $form['establishment_id'] = [
        '#type' => 'markup',
        '#markup' => $markup,
      ];

      return $form;
    }

    foreach ($establishments as $establishment) {
      $options[$establishment['establishment_id_guestapp']] = $establishment['establishment_name'] . ' (ID : ' . $establishment['establishment_id_guestapp'] . ')';
    }

    $default_value = NULL;

    if (isset($config['establishment_id'])) {
      $default_value = $config['establishment_id'];
    }
    elseif (count($options) === 1) {
      $default_value = array_key_first($options);
    }

    $form['establishment_id'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#title' => $this->t('Establishment'),
      '#required' => TRUE,
      '#default_value' => $default_value,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if (!$form_state->getValue('establishment_id')) {
      $url = Url::fromRoute('guest_suite.configuration')->toString();
      $message = Markup::create(
        $this->t('No establishment available.') . '<br>' .
        $this->t('Please review <a href="@url" title="Guest Suite settings">your settings</a>.', ['@url' => $url]
      ));
      $form_state->setErrorByName('establishment_id', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $this->configuration['establishment_id'] = $form_state->getValue('establishment_id');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'block__guest_suite__establishment_data',
      '#establishment' => $this->apiConsumer->getEstablishment($this->configuration['establishment_id']),
    ];
  }

}
