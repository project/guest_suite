<?php

declare(strict_types=1);

namespace Drupal\guest_suite\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\guest_suite\ApiConsumerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fetches a reviews page and enqueue them for import.
 *
 * @QueueWorker(
 *  id = "guest_suite_review_fetcher",
 *  title = @Translation("Import a Guest Suite Review"),
 *  cron = {"time" = 60}
 * )
 */
class ReviewFetcher extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\guest_suite\ReviewStorageInterface
   */
  protected $reviewStorage;

  /**
   * The API consumer.
   *
   * @var \Drupal\guest_suite\ApiConsumerInterface
   */
  protected $apiConsumer;

  /**
   * The importer queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $importerQueue;

  /**
   * Contructs a new ReviewImporter instance.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\guest_suite\ApiConsumerInterface $api_consumer
   *   The API consumer.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ApiConsumerInterface $api_consumer, QueueFactory $queue_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->reviewStorage = $entity_type_manager->getStorage('guest_suite_review');
    $this->apiConsumer = $api_consumer;
    $this->importerQueue = $queue_factory->get('guest_suite_review_importer');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('guest_suite.api_consumer'),
      $container->get('queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $reviews = $this->apiConsumer->getReviewsPerPage($data);

    if (!empty($reviews)) {
      foreach ($reviews as $review) {
        $this->importerQueue->createItem($review);
      }
    }
  }

}
