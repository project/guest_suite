<?php

declare(strict_types=1);

namespace Drupal\guest_suite\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Imports a Guest Suite review.
 *
 * @QueueWorker(
 *  id = "guest_suite_review_importer",
 *  title = @Translation("Import a Guest Suite Review"),
 *  cron = {"time" = 60}
 * )
 */
class ReviewImporter extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The review storage.
   *
   * @var \Drupal\guest_suite\ReviewStorageInterface
   */
  protected $reviewStorage;

  /**
   * Contructs a new ReviewImporter instance.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->reviewStorage = $entity_type_manager->getStorage('guest_suite_review');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $this->reviewStorage->import($data);
  }

}
