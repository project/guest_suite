<?php

declare(strict_types=1);

namespace Drupal\guest_suite\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\guest_suite\ApiConsumer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure guest_suite settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'guest_suite.settings';

  /**
   * The API consumer.
   *
   * @var \Drupal\guest_suite\ApiConsumer
   */
  protected $apiConsumer;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\guest_suite\ApiConsumer $api_consumer
   *   The Guest Suite API consumer.
   */
  public function __construct(ApiConsumer $api_consumer) {
    $this->apiConsumer = $api_consumer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('guest_suite.api_consumer'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'guest_suite_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config(static::SETTINGS);

    $form['access_token'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Access token'),
      '#description' => $this->t('Your Guest suite API access token.'),
      '#default_value' => $config->get('access_token') ?? NULL,
    ];

    $form['cron_import'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically imports latest reviews (beta)'),
      '#description' => $this->t('Tries to import reviews each time the CRON is run. (to avoid duplicated items in queue, reviews can take multiple CRON run to be fully imported)'),
      '#default_value' => $config->get('cron_import') ?? NULL,
    ];

    if ($stats = $this->apiConsumer->getStatistics()) {
      $form['info_fieldset'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Your global statistics (all establishments)'),
      ];

      $form['info_fieldset']['average_rate'] = [
        '#type' => 'item',
        '#title' => $this->t('Average rate'),
        '#markup' => $stats['average_rate'],
      ];
      $form['info_fieldset']['total'] = [
        '#type' => 'item',
        '#title' => $this->t('Reviews total number'),
        '#markup' => $stats['total'],
      ];
    }

    $form['delete_wrapper'] = [
      '#type' => 'details',
      '#title' => $this
        ->t('Reviews deletion'),
    ];

    $form['delete_wrapper']['delete'] = [
      '#type' => 'submit',
      '#submit' => [[$this, 'deleteContents']],
      '#value' => $this->t('Delete all reviews'),
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => [
          'button--danger',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->apiConsumer->setAccessToken($form_state->getValue('access_token'));
    if ($this->apiConsumer->getEstablishments() === NULL) {
      $url = Url::fromRoute('dblog.overview')->toString();
      $message = Markup::create(
        $this->t('An error occurred while requesting the API.') . '<br>' .
        $this->t('Please check <a href="@url" title="Guest Suite settings">your logs</a>.', ['@url' => $url]
      ));
      $form_state->setErrorByName('access_token', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('access_token', $form_state->getValue('access_token'))
      ->set('cron_import', $form_state->getValue('cron_import'))
      ->save();
  }

  /**
   * Deletes all reviews.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function deleteContents(array &$form, FormStateInterface $form_state): void {
    $batch = [
      'title' => $this->t('Deleting Guest suite reviews'),
      'error_message' => $this->t('An error occurred during processing.'),
      'operations' => [
        [[$this::class, 'batchFetch'], [[]]],
      ],
      'finished' => [$this::class, 'batchFinish'],
    ];

    batch_set($batch);
  }

  /**
   * Implements callback_batch_operation().
   */
  public static function batchFetch($multiple_params, &$context) {
    $storage = \Drupal::entityTypeManager()->getStorage('guest_suite_review');
    $step = 50;

    if (empty($context['sandbox']['rids'])) {
      $context['sandbox']['rids'] = \Drupal::entityQuery('guest_suite_review')
        ->accessCheck()
        ->execute();
      $context['results'] = [
        'success_count' => 0,
        'error_count' => 0,
      ];
      $context['sandbox']['review_index'] = 0;
    }

    if (!$count = count($context['sandbox']['rids'])) {
      return;
    }

    $index = &$context['sandbox']['review_index'];
    $rids = array_slice($context['sandbox']['rids'], $index, $step);
    $reviews = $storage->loadMultiple($rids);
    $index += count($rids);
    $context['message'] = \Drupal::service('string_translation')->formatPlural(
      ($index),
      'Deleting 1 of @total reviews.',
      'Deleting @count of @total reviews.',
      ['@total' => $count]
    );
    $context['finished'] = $index / $count;

    try {
      $storage->delete($reviews);
      $context['results']['success_count'] += count($reviews);
    }
    catch (\Exception $e) {
      $context['results']['error_count'] += count($reviews);
    }
  }

  /**
   * Implements callback_batch_finished().
   */
  public static function batchFinish($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    $string_translation = \Drupal::service('string_translation');

    if ($success) {
      if ($results['success_count'] === 0 && $results['error_count'] === 0) {
        $messenger->addStatus($string_translation->translate('No reviews to delete.'));
        return;
      }

      if ($results['success_count']) {
        $messenger->addStatus(
          $string_translation->formatPlural(
            $results['success_count'],
            '1 review was deleted.',
            '@count reviews were deleted.',
            ['@count' => $results['success_count']]
          )
        );
      }
      if ($results['error_count']) {
        $messenger->addError(
          $string_translation->formatPlural(
            $results['error_count'],
            'Failed to delete 1 review.',
            'Failed to delete @count reviews.',
            ['@count' => $results['error_count']]
          )
        );
      }

      return;
    }

    $messenger->addStatus(
      $string_translation->translate('An error occured during the deletion process.')
    );
  }

}
