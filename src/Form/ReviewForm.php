<?php

declare(strict_types=1);

namespace Drupal\guest_suite\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the guest suite review entity edit forms.
 */
class ReviewForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toString();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $link];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New guest suite review %label has been created.', $message_arguments));
      $this->logger('guest_suite')->notice('Created new guest suite review %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The guest suite review %label has been updated.', $message_arguments));
      $this->logger('guest_suite')->notice('Updated new guest suite review %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.guest_suite_review.canonical', ['guest_suite_review' => $entity->id()]);
  }

}
