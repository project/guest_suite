<?php

namespace Drupal\guest_suite\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\guest_suite\ApiConsumer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form for manually importing guest suite reviews.
 */
class ManualImportForm extends ContentEntityConfirmFormBase {

  /**
   * The Guest suite review storage.
   *
   * @var ReviewStorage
   */
  protected $reviewStorage;

  /**
   * The Guest Suite API consumer.
   *
   * @var \Drupal\guest_suite\ApiConsumer
   */
  protected $apiConsumer;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new ManualImportForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\guest_suite\ApiConsumer $api_consumer
   *   The Guest Suite API consumer.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, EntityTypeManagerInterface $entity_type_manager, ApiConsumer $api_consumer, ConfigFactory $config_factory, MessengerInterface $messenger) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->reviewStorage = $entity_type_manager->getStorage('guest_suite_review');
    $this->apiConsumer = $api_consumer;
    $this->config = $config_factory->getEditable('guest_suite.settings');
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager'),
      $container->get('guest_suite.api_consumer'),
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to manually import Guest Suite reviews from API?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Manually import reviews');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.guest_suite_review.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl(Url::fromRoute('entity.guest_suite_review.collection'));

    $statistics = $this->apiConsumer->getStatistics();
    if (empty($statistics['total'])) {
      $this->messenger->addStatus($this->t('No review to found'));
      return;
    }

    batch_set([
      'title' => $this->t('Reviews import'),
      'error_message' => $this->t('An error occurred during processing.'),
      'operations' => [
        [[$this::class, 'batchFetch'], [['reviews_total' => $statistics['total']]]],
        [[$this::class, 'batchImport'], [[]]],
      ],
      'finished' => [$this::class, 'batchFinish'],
    ]);
  }

  /**
   * Implements callback_batch_operation().
   */
  public static function batchfetch($multiple_params, &$context) {
    // Init. context values.
    if (empty($context['sandbox'])) {
      $context['sandbox']['pages_total'] = ceil($multiple_params['reviews_total'] / 100);
      $context['sandbox']['page_index'] = 0;
      $context['results']['reviews'] = [];
      $context['results']['reviews_total'] = $multiple_params['reviews_total'];
    }
    // Fetch reviews.
    $page_index = &$context['sandbox']['page_index'];
    $reviews = \Drupal::service('guest_suite.api_consumer')
      ->getReviewsPerPage($page_index);
    $context['results']['reviews'] = array_merge($context['results']['reviews'], $reviews);
    $context['message'] = \Drupal::service('string_translation')->formatPlural(
      ($page_index + 1),
      'Fetched 1 reviews page of @total.',
      'Fetched @count reviews pages of @total.',
      ['@total' => $context['sandbox']['pages_total']]
    );
    $context['finished'] = ($page_index + 1) / $context['sandbox']['pages_total'];
    $page_index++;
  }

  /**
   * Implements callback_batch_operation().
   */
  public static function batchImport($multiple_params, &$context) {
    if (empty($context['results']['reviews'])) {
      return;
    }
    // Init. context values.
    if (empty($context['sandbox'])) {
      $context['sandbox']['review_index'] = 0;
      $context['results']['created'] = $context['results']['updated'] = 0;
    }
    // Import reviews.
    $string_translation = \Drupal::service('string_translation');
    $review_index = &$context['sandbox']['review_index'];
    $review_import = \Drupal::entityTypeManager()
      ->getStorage('guest_suite_review')
      ->import($context['results']['reviews'][$review_index]);
    $context['message'] = $string_translation->formatPlural(
      ($review_index + 1),
      'Imported 1 review of @total.',
      'Imported @count reviews of @total.',
      ['@total' => $context['results']['reviews_total']]
    );

    switch ($review_import) {
      case SAVED_NEW:
        $context['results']['created']++;
        break;

      case SAVED_UPDATED:
        $context['results']['updated']++;
        break;
    }

    $context['finished'] = ($review_index + 1) / $context['results']['reviews_total'];
    $review_index++;
  }

  /**
   * Implements callback_batch_finished().
   */
  public static function batchFinish($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    $string_translation = \Drupal::service('string_translation');

    if (!$success) {
      $messenger->addError(
        $string_translation->translate('An error occurred during processing.')
      );
      return;
    }

    if (!isset($results['created'])) {
      $url = Url::fromRoute('guest_suite.configuration')->toString();
      $messenger->addError(Markup::create(
        $string_translation->translate('Missing or invalid Guest Suite access token.') . '<br>' .
        $string_translation->translate('Please review <a href=":url" title="Guest Suite settings">your settings</a>.', [':url' => $url])
      ));
      return;
    }

    if (!$results['created']) {
      $messenger->addStatus($string_translation->translate('No review created.'));
    }
    else {
      $messenger->addStatus($string_translation->formatPlural(
        $results['created'],
        '1 review was successfully created.',
        '@count reviews were successfully created.',
      ));
    }

    if (!$results['updated']) {
      $messenger->addStatus($string_translation->translate('No review updated.'));
    }
    else {
      $messenger->addStatus($string_translation->formatPlural(
        $results['updated'],
        '1 review was successfully updated.',
        '@count reviews were successfully updated.',
      ));
    }
  }

}
