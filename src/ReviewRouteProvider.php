<?php

declare(strict_types=1);

namespace Drupal\guest_suite;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for the Guest Suite entity.
 */
class ReviewRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    if ($manual_import_route = $this->getManualImportRoute($entity_type)) {
      $collection->add("entity.{$entity_type->id()}.manual_import", $manual_import_route);
    }

    $collection->addRequirements([
      '_permission' => 'access guest suite review overview',
    ]);

    return $collection;
  }

  /**
   * Gets manual reviews import route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getManualImportRoute(EntityTypeInterface $entity_type) {
    $route = new Route($entity_type->getLinkTemplate('manual-import'));
    $route
      ->addDefaults([
        '_entity_form' => 'guest_suite_review.manual-import',
      ])
      ->setRequirement('guest_suite_review', '\d+')
      ->setOption('_admin_route', TRUE);

    return $route;
  }

}
