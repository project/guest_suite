<?php

declare(strict_types=1);

namespace Drupal\guest_suite;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for a guest suite review entity type.
 */
class ReviewViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode): array {
    $build = parent::getBuildDefaults($entity, $view_mode);
    // The guest suite review has no entity template itself.
    unset($build['#theme']);
    return $build;
  }

}
