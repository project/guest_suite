<?php

namespace Drupal\guest_suite;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed field link for the 'warning_url' guest_suite field.
 */
class ReviewWarningUrl extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\guest_suite\Entity\ReviewInterface $review */
    $review = $this->getEntity();
    $this->list[0] = $this->createItem(0, [
      'uri' => 'https://wire.guest-suite.com/review/warn/' . $review->getRemoteId() . '-' . $review->getUniqueToken(),
      'title' => $this->t('Report review'),
      'option' => [],
    ]);
  }

}
