<?php

declare(strict_types=1);

namespace Drupal\guest_suite;

/**
 * The Guest Suite API consumer interface.
 *
 * @package Drupal\guest_suite\Services
 */
interface ApiConsumerInterface {

  /**
   * Access token setter.
   *
   * @param string|null $access_token
   *   The Guest Suite access token.
   *
   * @return self
   *   The current object.
   */
  public function setAccessToken(?string $access_token = NULL): self;

  /**
   * Gets reviews per page from API.
   *
   * @param int $page
   *   The page number.
   *
   * @return array
   *   The array of reviews per page from API.
   */
  public function getReviewsPerPage(int $page): ?array;

  /**
   * Gets all establishments from API.
   *
   * @return array
   *   The array of all establishments and their infos from API.
   */
  public function getEstablishments(): ?array;

  /**
   * Gets establishment with the establishment id guestapp from API.
   *
   * @param int $establishment_id_guestapp
   *   The establishment id guestapp.
   *
   * @return array
   *   The array of all infos about the given establishment.
   */
  public function getEstablishment(int $establishment_id_guestapp): ?array;

  /**
   * Gets statistics for all establishments from API.
   *
   * @return array
   *   The array of global statistics of all establishments,
   *   containing the average rate and the total of reviews.
   */
  public function getStatistics(): ?array;

}
