<?php

declare(strict_types=1);

namespace Drupal\guest_suite\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Url;

/**
 * Provides an interface defining a guest suite review entity type.
 */
interface ReviewInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the guest suite review title.
   *
   * @return string
   *   Title of the guest suite review.
   */
  public function getTitle(): ?string;

  /**
   * Sets the guest suite review title.
   *
   * @param string $title
   *   The guest suite review title.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setTitle($title): self;

  /**
   * Gets the guest suite review remote ID.
   *
   * @return int
   *   Remote ID of the guest suite review.
   */
  public function getRemoteId(): int;

  /**
   * Sets the guest suite review remote ID.
   *
   * @param int $remote_id
   *   The guest suite review remote ID.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setRemoteId($remote_id): self;

  /**
   * Gets the guest suite review guestapp ID.
   *
   * @return int
   *   Guestapp ID of the guest suite review.
   */
  public function getGuestappId(): int;

  /**
   * Sets the guest suite review guestapp ID.
   *
   * @param int $guestapp_id
   *   The guest suite review guestapp ID.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setGuestappId($guestapp_id): self;

  /**
   * Gets the guest suite review creation timestamp.
   *
   * @return int
   *   Creation timestamp of the guest suite review.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the guest suite review creation timestamp.
   *
   * @param string $timestamp
   *   The guest suite review creation timestamp.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setCreatedTime($timestamp): self;

  /**
   * Gets the guest suite review user name.
   *
   * @return string
   *   User name of the guest suite review.
   */
  public function getUserName(): string;

  /**
   * Sets the guest suite review user name.
   *
   * @param string $user_name
   *   The guest suite review user name.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setUserName($user_name): self;

  /**
   * Gets the guest suite review comment.
   *
   * @return string
   *   Comment of the guest suite review.
   */
  public function getComment(): string;

  /**
   * Sets the guest suite review comment.
   *
   * @param string $comment_text
   *   The guest suite review comment.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setComment($comment_text): self;

  /**
   * Gets the guest suite review creation date.
   *
   * @return string
   *   Creation date of the guest suite review.
   */
  public function getCreationDate(): string;

  /**
   * Sets the guest suite review creation date.
   *
   * @param string $creation_date
   *   The guest suite review creation date.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setCreationDate($creation_date): self;

  /**
   * Gets the guest suite review publication date.
   *
   * @return string
   *   Publication date of the guest suite review.
   */
  public function getPublicationDate(): string;

  /**
   * Sets the guest suite review publication date.
   *
   * @param string $publication_date
   *   The guest suite review publication date.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setPublicationDate($publication_date): self;

  /**
   * Gets the guest suite review experience date.
   *
   * @return string
   *   Experience date of the guest suite review.
   */
  public function getExperienceDate(): ?string;

  /**
   * Sets the guest suite review experience date.
   *
   * @param string $experience_date
   *   The guest suite review experience date.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setExperienceDate($experience_date): self;

  /**
   * Gets the guest suite review type.
   *
   * @return string
   *   Type of the guest suite review.
   */
  public function getType(): ?string;

  /**
   * Sets the guest suite review type.
   *
   * @param string $type
   *   The guest suite review type.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setType($type): self;

  /**
   * Gets the guest suite review global rate.
   *
   * @return int
   *   Global rate of the guest suite review.
   */
  public function getGlobalRate(): int;

  /**
   * Sets the guest suite review global rate.
   *
   * @param int $global_rate
   *   The guest suite review global rate.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setGlobalRate($global_rate): self;

  /**
   * Gets the guest suite review establishment id.
   *
   * @return string
   *   Establishment id of the guest suite review.
   */
  public function getEstablishmentId(): string;

  /**
   * Sets the guest suite review establishment id.
   *
   * @param string $establishment_id
   *   The guest suite review establishment id.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setEstablishmentId($establishment_id): self;

  /**
   * Gets the guest suite review establishment id guestapp.
   *
   * @return int
   *   Establishment id guestapp of the guest suite review.
   */
  public function getEstablishmentIdGuestapp(): int;

  /**
   * Sets the guest suite review establishment id guestapp.
   *
   * @param int $establishment_id_guestapp
   *   The guest suite review establishment id guestapp.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setEstablishmentIdGuestapp($establishment_id_guestapp): self;

  /**
   * Gets the guest suite review authenticity URL.
   *
   * @return \Drupal\Core\Url
   *   Authenticity URL of the guest suite review.
   */
  public function getAuthenticityUrl(): Url;

  /**
   * Sets the guest suite review authenticity URL.
   *
   * @param string $authenticity_url
   *   The guest suite review authenticity URL.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setAuthenticityUrl($authenticity_url): self;

  /**
   * Gets the guest suite review unique token.
   *
   * @return string|null
   *   Unique token of the guest suite review or NULL if empty.
   */
  public function getUniqueToken(): ?string;

  /**
   * Sets the guest suite review unique token.
   *
   * @param string $unique_token
   *   The guest suite review unique token.
   *
   * @return \Drupal\guest_suite\Entity\ReviewInterface
   *   The called guest suite review entity.
   */
  public function setUniqueToken($unique_token): self;

}
