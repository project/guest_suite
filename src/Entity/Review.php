<?php

declare(strict_types=1);

namespace Drupal\guest_suite\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\guest_suite\ReviewWarningUrl;

/**
 * Defines the guest suite review entity class.
 *
 * @ContentEntityType(
 *   id = "guest_suite_review",
 *   label = @Translation("Guest suite review"),
 *   label_collection = @Translation("Guest suite reviews"),
 *   handlers = {
 *     "storage" = "Drupal\guest_suite\ReviewStorage",
 *     "view_builder" = "Drupal\guest_suite\ReviewViewBuilder",
 *     "list_builder" = "Drupal\guest_suite\ReviewListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\guest_suite\Form\ReviewForm",
 *       "edit" = "Drupal\guest_suite\Form\ReviewForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "manual-import" = "Drupal\guest_suite\Form\ManualImportForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\guest_suite\ReviewRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     }
 *   },
 *   field_indexes = {
 *     "remote_id",
 *     "guestapp_id",
 *   },
 *   base_table = "guest_suite_review",
 *   data_table = "guest_suite_review_field_data",
 *   translatable = TRUE,
 *   admin_permission = "access guest suite review overview",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/content/guest-suite-review/add",
 *     "manual-import" = "/admin/content/guest-suite-review/manual-import",
 *     "canonical" = "/guest_suite_review/{guest_suite_review}",
 *     "edit-form" = "/admin/content/guest-suite-review/{guest_suite_review}/edit",
 *     "delete-form" = "/admin/content/guest-suite-review/{guest_suite_review}/delete",
 *     "delete-multiple-form" = "/admin/content/guest-suite-reviews/delete",
 *     "collection" = "/admin/content/guest-suite-reviews",
 *   },
 *   field_ui_base_route = "guest_suite.configuration",
 * )
 */
class Review extends ContentEntityBase implements ReviewInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle(): ?string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title): self {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId(): int {
    return (int) $this->get('remote_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteId($remote_id): self {
    $this->set('remote_id', $remote_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGuestappId(): int {
    return (int) $this->get('guestapp_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGuestappId($guestapp_id): self {
    $this->set('guestapp_id', $guestapp_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): self {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserName(): string {
    return $this->get('user_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserName($user_name): self {
    $this->set('user_name', $user_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getComment(): string {
    return $this->get('comment_text')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setComment($comment_text): self {
    $this->set('comment_text', $comment_text);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreationDate(): string {
    return $this->get('creation_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreationDate($creation_date): self {
    $this->set('creation_date', $creation_date);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublicationDate(): string {
    return $this->get('publication_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublicationDate($publication_date): self {
    $this->set('publication_date', $publication_date);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExperienceDate(): ?string {
    return $this->get('experience_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setExperienceDate($experience_date): self {
    $this->set('experience_date', $experience_date);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): ?string {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setType($type): self {
    $this->set('type', $type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGlobalRate(): int {
    return $this->get('global_rate')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGlobalRate($global_rate): self {
    $this->set('global_rate', $global_rate);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEstablishmentId(): string {
    return $this->get('establishment_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEstablishmentId($establishment_id): self {
    $this->set('establishment_id', $establishment_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEstablishmentIdGuestapp(): int {
    return (int) $this->get('establishment_id_guestapp')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEstablishmentIdGuestapp($establishment_id_guestapp): self {
    $this->set('establishment_id_guestapp', $establishment_id_guestapp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticityUrl(): Url {
    return $this->get('authenticity_url')->first()->getUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function setAuthenticityUrl($authenticity_url): self {
    $this->set('authenticity_url', $authenticity_url);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUniqueToken(): ?string {
    return $this->get('unique_token')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUniqueToken($unique_token): self {
    $this->set('unique_token', $unique_token);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['remote_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Remote ID'))
      ->setDescription(t('The remote ID.'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['guestapp_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Guestapp ID'))
      ->setDescription(t('The guestapp ID.'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->addConstraint('UniqueField');

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the guest suite review is published.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Published')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the guest suite review was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the guest suite review was last edited.'));

    $fields['user_name'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('User name'))
      ->setDescription(t('Nickname of the review writer.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string_long')
      ->setTranslatable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the review.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -5,
        'settings' => [
          'rows' => 3,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['comment_text'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Comment'))
      ->setDescription(t('The text review.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['creation_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Creation date'))
      ->setDescription(t('Date and time when the review has been written.'))
      ->setSetting('datetime_type', 'datetime')
      ->setDisplayConfigurable('view', TRUE);

    $fields['publication_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Publication date'))
      ->setDescription(t('Date and time when the review has been published.'))
      ->setSetting('datetime_type', 'datetime')
      ->setDisplayConfigurable('view', TRUE);

    $fields['experience_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Experience date'))
      ->setDescription(t('Date when the experience has occured.'))
      ->setSetting('datetime_type', 'date')
      ->setDisplayConfigurable('view', TRUE);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Type'))
      ->setDescription(t('Kind of stay.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['global_rate'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Global rate'))
      ->setDescription(t('Global rate of the review.'))
      ->setSetting('max', 10)
      ->setSetting('unsigned', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['establishment_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Establishment ID'))
      ->setDescription(t('Identifier (slug) of the establishment in your system.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['establishment_id_guestapp'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Establishment ID guestapp'))
      ->setDescription(t('Identifier of the establishment on Guest Suite.'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['authenticity_url'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Authenticity url'))
      ->setDescription(t('The authenticity url.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['unique_token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Unique token'))
      ->setDescription(t('Used for specific URL such as warning.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['warning_url'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Warning URL'))
      ->setDescription(t('The link to warn the review.'))
      ->setComputed(TRUE)
      ->setClass(ReviewWarningUrl::class)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
