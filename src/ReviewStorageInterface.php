<?php

namespace Drupal\guest_suite;

/**
 * Defines the Guest Suite review storage.
 */
interface ReviewStorageInterface {

  /**
   * Import a review object from its remote data.
   *
   * @param array $remote_values
   *   Review remote values.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  public function import(array $remote_values = []): int;

}
