<?php

/**
 * @file
 * Guest suite install file.
 */

declare(strict_types=1);

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Convert title field from string to string_long.
 */
function guest_suite_update_8101() {
  $database = \Drupal::database();
  $transaction = $database->startTransaction();

  $entity_type_manager = \Drupal::entityTypeManager();
  $bundle_of = 'guest_suite_review';
  $field_name = 'title';

  $storage = $entity_type_manager->getStorage($bundle_of);
  $bundle_definition = $entity_type_manager->getDefinition($bundle_of);
  $id_key = $bundle_definition->getKey('id');
  $table_name = $storage->getDataTable() ?: $storage->getBaseTable();
  $definition_manager = \Drupal::entityDefinitionUpdateManager();

  // Store the existing values.
  $field_values = $database->select($table_name)
    ->fields($table_name, [$id_key, $field_name])
    ->execute()
    ->fetchAllKeyed();

  // Clear out the values.
  $database->update($table_name)
    ->fields([$field_name => NULL])
    ->execute();

  // Uninstall the field.
  $field_storage_definition = $definition_manager->getFieldStorageDefinition($field_name, $bundle_of);
  $definition_manager->uninstallFieldStorageDefinition($field_storage_definition);

  // Create a new field definition.
  $new_field = BaseFieldDefinition::create('string_long')
    ->setTranslatable(TRUE)
    ->setLabel(t('Title'))
    ->setDescription(t('The title of the review.'))
    ->setDisplayOptions('form', [
      'type' => 'string_textarea',
      'weight' => -5,
      'settings' => [
        'rows' => 3,
      ],
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

  // Install the new definition.
  $definition_manager->installFieldStorageDefinition($field_name, $bundle_of, $bundle_of, $new_field);

  // Restore the values.
  foreach ($field_values as $id => $value) {
    $database->update($table_name)
      ->fields([$field_name => $value])
      ->condition($id_key, $id)
      ->execute();
  }

  // Commit transaction.
  unset($transaction);
}

/**
 * Convert guest_suite_review entity to publishable.
 */
function guest_suite_update_8102() {
  $definition_update_manager = \Drupal::entityDefinitionUpdateManager();
  $entity_type_id = 'guest_suite_review';
  $field_name = 'status';

  // Update the entity keys.
  $entity_type = $definition_update_manager->getEntityType($entity_type_id);
  $entity_keys = $entity_type->getKeys();
  $entity_keys['published'] = $field_name;
  $entity_type->set('entity_keys', $entity_keys);
  $definition_update_manager->updateEntityType($entity_type);

  /** @var \Drupal\Core\Field\BaseFieldDefinition $field_storage_definition */
  $field_storage_definition = $definition_update_manager->getFieldStorageDefinition($field_name, $entity_type_id);
  $field_storage_definition->setDescription(t('A boolean indicating whether the guest suite review is published.'));
  $field_storage_definition->setSetting('on_label', 'Published');
  $definition_update_manager->updateFieldStorageDefinition($field_storage_definition);

  return t('Converting @entity_type_label to publishable.', [
    '@entity_type_label' => $entity_type->getLabel(),
  ]);
}
