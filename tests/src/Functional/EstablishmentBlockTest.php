<?php

namespace Drupal\Tests\guest_suite\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the configuration options and block created by Guest suite module.
 */
class EstablishmentBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';


  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'guest_suite_test',
  ];

  /**
   * Tests guest_suite_establishment functionality.
   */
  public function testEstablishmentBlock() {
    $assert = $this->assertSession();

    // Create user.
    $web_user = $this->drupalCreateUser(['administer blocks']);
    // Login the admin user.
    $this->drupalLogin($web_user);

    $theme_name = $this->config('system.theme')->get('default');

    // Verify the block is listed to be added.
    $this->drupalGet('/admin/structure/block/library/' . $theme_name, ['query' => ['region' => 'content']]);
    $assert->pageTextContains('Guest Suite establishment');

    // Define and place the block.
    $block_settings = [
      'id' => 'guest_suite_guest_suite_establishment',
      'theme' => $theme_name,
      'establishment_id' => '123',
    ];
    $this->drupalPlaceBlock('guest_suite_establishment', $block_settings);

    // Verify that the block is here.
    $this->drupalGet('');
    $assert->pageTextContains('Establishment test 1');

    // Change selected establishment.
    $this->drupalGet('/admin/structure/block/manage/' . $block_settings['id']);
    $assert->pageTextContains('Establishment test 1 (ID : 123)');
    $assert->pageTextContains('Establishment test 2 (ID : 456)');
    $edit = [
      'settings[establishment_id]' => '456',
    ];
    $this->submitForm($edit, 'Save block');
    $assert->statusCodeEquals(200);

    // Verify that new content is shown.
    $this->drupalGet('');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains('Establishment test 2');
  }

}
