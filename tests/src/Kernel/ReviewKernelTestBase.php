<?php

namespace Drupal\Tests\guest_suite\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Provides a base class for review kernel tests.
 */
abstract class ReviewKernelTestBase extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'guest_suite',
    'datetime',
    'link',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('guest_suite_review');
  }

}
