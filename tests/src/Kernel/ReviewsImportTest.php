<?php

namespace Drupal\Tests\guest_suite\Kernel;

use Drupal\guest_suite\Entity\Review;

/**
 * Tests reviews import.
 *
 * @group commerce
 */
class ReviewsImportTest extends ReviewKernelTestBase {

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The review storage.
   *
   * @var \Drupal\guest_suite\ReviewStorage
   */
  protected $reviewStorage;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'guest_suite_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $user = $this->createUser();
    $this->user = $this->reloadEntity($user);
    $this->reviewStorage = $this->container->get('entity_type.manager')->getStorage('guest_suite_review');
    $this->config = $this->container->get('config.factory')->getEditable('guest_suite.settings');
  }

  /**
   * Tests reviews import being created or updated (with cron and queue worker).
   */
  public function testReviewsImport() {
    $fetchQueue = $this->container->get('queue')->get('guest_suite_review_fetcher');
    $importQueue = $this->container->get('queue')->get('guest_suite_review_importer');

    // By default, reviews import is disabled.
    // Confirm that no reviews are queued.
    $this->container->get('guest_suite.cron')->run();
    $this->assertEquals(0, $fetchQueue->numberOfItems());
    $this->assertEquals(0, $importQueue->numberOfItems());

    // Enable reviews import.
    $this->config->set('cron_import', TRUE);
    $this->config->save();

    // Create review1 that must be updated.
    $review1 = Review::create([
      'guestapp_id' => 1,
    ]);
    $review1->save();

    // Confirm that cron has queued page IDs.
    $this->container->get('guest_suite.cron')->run();
    $this->assertEquals(2, $fetchQueue->numberOfItems());
    $this->assertEquals(0, $importQueue->numberOfItems());

    // Confirm that items are not duplicated in fetch queue.
    $this->container->get('guest_suite.cron')->run();
    $this->assertEquals(2, $fetchQueue->numberOfItems());
    $this->assertEquals(0, $importQueue->numberOfItems());

    // Confirm that fetch queue has queued reviews IDs.
    $fetchQueue->deleteQueue();
    $this->container->get('cron')->run();
    $this->assertEquals(0, $fetchQueue->numberOfItems());
    $this->assertEquals(110, $importQueue->numberOfItems());

    // Confirm that items are not duplicated in import queue.
    $this->container->get('guest_suite.cron')->run();
    $this->assertEquals(0, $fetchQueue->numberOfItems());
    $this->assertEquals(110, $importQueue->numberOfItems());

    // Run Cron and confirm that 110 reviews were created/updated.
    // @see guest_suite/tests/modules/guest_suite_test/src/ApiConsumerOverride
    $this->container->get('cron')->run();
    $this->reviewStorage->resetCache();
    $reviews = $this->reviewStorage->getQuery()->accessCheck(FALSE)->execute();
    $this->assertEquals(110, count($reviews));

    // Confirm that review1 was updated.
    /** @var \Drupal\guest_suite\Entity\ReviewInterface $review1 */
    $review1 = $this->reviewStorage->load(1);
    $this->assertNull($review1->get('remote_id')->value);
    $this->assertEquals(1, $review1->get('guestapp_id')->value);
    $this->assertEquals("en", $review1->get('langcode')->value);
    $this->assertEquals("Internet user", $review1->get('user_name')->value);
    $this->assertNull($review1->get('title')->value);
    $this->assertEquals("Great !", $review1->get('comment_text')->value);
    $this->assertNull($review1->get('creation_date')->value);
    $this->assertEquals("2020-10-08T10:37:24", $review1->get('publication_date')->value);
    $this->assertEquals("2020-10-08", $review1->get('experience_date')->value);
    $this->assertNull($review1->get('type')->value);
    $this->assertEquals(10, $review1->get('global_rate')->value);
    $this->assertEquals("test-123", $review1->get('establishment_id')->value);
    $this->assertEquals(123, $review1->get('establishment_id_guestapp')->value);
    $this->assertNull($review1->get('authenticity_url')->value);
    $this->assertNull($review1->get('unique_token')->value);

    // Confirm that review2 was created.
    /** @var \Drupal\guest_suite\Entity\ReviewInterface $review2 */
    $review2 = $this->reviewStorage->load(2);
    $this->assertEquals(2, $review2->get('remote_id')->value);
    $this->assertEquals(2, $review2->get('guestapp_id')->value);
    $this->assertEquals("fr", $review2->get('langcode')->value);
    $this->assertEquals("Some other user", $review2->get('user_name')->value);
    $this->assertEquals("I don't get the hype", $review2->get('title')->value);
    $this->assertEquals("I have no idea what thee fuss is about.", $review2->get('comment_text')->value);
    $this->assertEquals("2020-10-08T10:12:33", $review2->get('creation_date')->value);
    $this->assertEquals("2020-10-08T10:12:33", $review2->get('publication_date')->value);
    $this->assertEquals("2020-10-08", $review2->get('experience_date')->value);
    $this->assertNull($review2->get('type')->value);
    $this->assertEquals(3, $review2->get('global_rate')->value);
    $this->assertEquals("test-456", $review2->get('establishment_id')->value);
    $this->assertEquals(456, $review2->get('establishment_id_guestapp')->value);
    $this->assertEquals(
      "https://guestapp.me/review/some-url",
      $review2->get('authenticity_url')
        ->first()
        ->getUrl()
        ->toString()
    );
    $this->assertEquals("someOtherToken", $review2->get('unique_token')->value);
  }

}
