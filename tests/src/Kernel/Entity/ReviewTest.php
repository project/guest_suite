<?php

namespace Drupal\Tests\guest_suite\Kernel\Entity;

use Drupal\Core\Url;
use Drupal\guest_suite\Entity\Review;
use Drupal\Tests\guest_suite\Kernel\ReviewKernelTestBase;

/**
 * Tests the Review entity.
 *
 * @coversDefaultClass \Drupal\guest_suite\Entity\Review
 */
class ReviewTest extends ReviewKernelTestBase {

  /**
   * Tests the review entity and its methods.
   *
   * @covers ::getTitle
   * @covers ::setTitle
   * @covers ::getRemoteId
   * @covers ::setRemoteId
   * @covers ::getGuestappId
   * @covers ::setGuestappId
   * @covers ::isPublished
   * @covers ::getCreatedTime
   * @covers ::setCreatedTime
   * @covers ::getUserName
   * @covers ::setUserName
   * @covers ::getComment
   * @covers ::setComment
   * @covers ::getCreationDate
   * @covers ::setCreationDate
   * @covers ::getPublicationDate
   * @covers ::setPublicationDate
   * @covers ::getExperienceDate
   * @covers ::setExperienceDate
   * @covers ::getType
   * @covers ::setType
   * @covers ::getGlobalRate
   * @covers ::setGlobalRate
   * @covers ::getEstablishmentId
   * @covers ::setEstablishmentId
   * @covers ::getEstablishmentIdGuestapp
   * @covers ::setEstablishmentIdGuestapp
   * @covers ::getAuthenticityUrl
   * @covers ::setAuthenticityUrl
   * @covers ::getUniqueToken
   * @covers ::setUniqueToken
   */
  public function testReview() {
    /** @var \Drupal\guest_suite\Entity\ReviewInterface $review */
    $review = Review::create();
    $review->save();

    $review->setTitle('Test');
    $this->assertEquals('Test', $review->getTitle());

    $review->setRemoteId('0001');
    $this->assertEquals('0001', $review->getRemoteId());
    $review->setGuestappId('0001');
    $this->assertEquals('0001', $review->getGuestappId());

    $review->setPublished();
    $this->assertEquals(1, $review->isPublished());

    $review->setCreatedTime(\Drupal::time()->getCurrentTime());
    $this->assertEquals(
      \Drupal::time()->getCurrentTime(),
      $review->getCreatedTime()
    );

    $review->setUserName('User test');
    $this->assertEquals('User test', $review->getUserName());
    $review->setComment('Comment test');
    $this->assertEquals('Comment test', $review->getComment());

    $review->setCreationDate('2017-07-05T08:10:36');
    $this->assertEquals('2017-07-05T08:10:36', $review->getCreationDate());
    $review->setPublicationDate('2017-07-05T08:10:40');
    $this->assertEquals('2017-07-05T08:10:40', $review->getPublicationDate());
    $review->setExperienceDate('2017-07-04');
    $this->assertEquals('2017-07-04', $review->getExperienceDate());

    $review->setType('family');
    $this->assertEquals('family', $review->getType());

    $review->setGlobalRate(7);
    $this->assertEquals(7, $review->getGlobalRate());

    $review->setEstablishmentId('1234567890');
    $this->assertEquals('1234567890', $review->getEstablishmentId());
    $review->setEstablishmentIdGuestapp(1234567890);
    $this->assertEquals(1234567890, $review->getEstablishmentIdGuestapp());

    $review->setAuthenticityUrl('https://guestapp.me/review/some-url');
    $this->assertEquals(
      Url::fromUri('https://guestapp.me/review/some-url'),
      $review->getAuthenticityUrl()
    );
    $review->setUniqueToken('testToken');
    $this->assertEquals('testToken', $review->getUniqueToken());
  }

}
