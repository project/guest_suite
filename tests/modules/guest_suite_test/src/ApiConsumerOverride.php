<?php

declare(strict_types=1);

namespace Drupal\guest_suite_test;

use Drupal\guest_suite\ApiConsumer;

/**
 * The service that overrides the API consumer in tests.
 *
 * @package Drupal\guest_suite_test\Services
 */
class ApiConsumerOverride extends ApiConsumer {

  /**
   * {@inheritDoc}
   */
  public function getReviewsPerPage(int $page): ?array {
    $reviews = [];

    switch ($page) {
      case 0:
        $init_id = 2;
        $end_id = 100;
        $reviews[] = [
          "id" => 1,
          "guestapp_id" => 1,
          "language_code" => "FR",
          "user_name" => "Internet user",
          "title" => NULL,
          "comment" => "Great !",
          "creation_date" => "2020-10-08T10:37:24+00:00",
          "creation_date_simple" => "20201008103724",
          "creation_date_timestamp" => 1602153444,
          "publication_date" => "2020-10-08T10:37:24+00:00",
          "experience_date" => "2020-10-08",
          "timezone" => "Europe/Paris",
          "type" => NULL,
          "global_rate" => 10,
          "ratings" => [],
          "ratings_by_language" => [],
          "establishment_id" => "test-123",
          "establishment_id_guestapp" => 123,
          "establishment_name" => "Establishment test 1",
          "establishment_address" => "1 Guest Suite Street",
          "establishment_city" => "Nantes",
          "establishment_zip" => "44000",
          "establishment_state" => NULL,
          "establishment_country" => "France",
          "establishment_type" => "hotel",
          "authenticity_url" => "https://guestapp.me/review/some-url",
          "responses" => [],
          "unique_token" => "someToken",
        ];
        break;

      case 1:
        $init_id = 101;
        $end_id = 110;
        break;

      default:
        return $reviews;
    }

    for ($i = $init_id; $i <= $end_id; $i++) {
      $reviews[] = [
        "id" => $i,
        "guestapp_id" => $i,
        "language_code" => "FR",
        "user_name" => "Some other user",
        "title" => "I don't get the hype",
        "comment" => "I have no idea what thee fuss is about.",
        "creation_date" => "2020-10-08T10:12:33+00:00",
        "creation_date_simple" => "20201008101233",
        "creation_date_timestamp" => 1602151953,
        "publication_date" => "2020-10-08T10:12:33+00:00",
        "experience_date" => "2020-10-08",
        "timezone" => "Europe/Paris",
        "type" => NULL,
        "global_rate" => 3,
        "ratings" => [],
        "ratings_by_language" => [],
        "establishment_id" => "test-456",
        "establishment_id_guestapp" => 456,
        "establishment_name" => "Establishment test 2",
        "establishment_address" => "2 Guest Suite Street",
        "establishment_city" => "Nantes",
        "establishment_zip" => "44000",
        "establishment_state" => NULL,
        "establishment_country" => "France",
        "establishment_type" => "hotel",
        "authenticity_url" => "https://guestapp.me/review/some-url",
        "responses" => [],
        "unique_token" => "someOtherToken",
      ];
    }

    return $reviews;
  }

  /**
   * {@inheritDoc}
   */
  public function getEstablishments(): ?array {
    return [
      [
        "establishment_id_guestapp" => 123,
        "establishment_name" => "Establishment test 1",
      ],
      [
        "establishment_id_guestapp" => 456,
        "establishment_name" => "Establishment test 2",
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getEstablishment(int $establishment_id_guestapp): ?array {
    if ($establishment_id_guestapp == '123') {
      $establishement = [
        "establishment_id_guestapp" => 123,
        "establishment_id" => "test-123",
        "establishment_name" => "Establishment test 1",
        "establishment_address" => "1 Guest Suite Street",
        "establishment_zip" => "44000",
        "establishment_city" => "Nantes",
        "establishment_country" => "France",
        "average_rate" => 8.79,
        "total_reviews" => 58,
      ];
    }
    elseif ($establishment_id_guestapp == '456') {
      $establishement = [
        "establishment_id_guestapp" => 456,
        "establishment_id" => "test-456",
        "establishment_name" => "Establishment test 2",
        "establishment_address" => "2 Guest Suite Street",
        "establishment_zip" => "44000",
        "establishment_city" => "Nantes",
        "establishment_country" => "France",
        "average_rate" => 8.79,
        "total_reviews" => 58,
      ];
    }

    return $establishement ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getStatistics(): ?array {
    return [
      "average_rate" => 8.79,
      "total" => "110",
    ];
  }

}
